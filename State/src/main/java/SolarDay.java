public class SolarDay {
    private State timeOfDay;

    public void setState(State state) {
        if(state==null){
            throw new IllegalArgumentException("Error!");
        }
        this.timeOfDay = state;
    }

    public void changeState() {
        if (timeOfDay instanceof Morning) {
            setState(new Day());
        } else if (timeOfDay instanceof Day) {
            setState(new Evening());
        } else if (timeOfDay instanceof Evening) {
            setState(new Night());
        } else if (timeOfDay instanceof Night) {
            setState(new Morning());
        }
    }

    public void displayTimeDay() {
        timeOfDay.timeOfDayDisplay();
    }
}
