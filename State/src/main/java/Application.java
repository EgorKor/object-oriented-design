public class Application {
    public static void main(String[] args) {
        SolarDay solarDay = new SolarDay();
        State state = new Day();

        solarDay.setState(state);

        for(int i=0;i<8;i++){
            solarDay.displayTimeDay();
            solarDay.changeState();
        }
    }
}
