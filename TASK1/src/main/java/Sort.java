import java.util.Arrays;

public class Sort {

    public void sort(String str, int[] arr) throws IllegalAccessException {
        SortFactory.createSort(str).sortArray(arr);
    }
}
