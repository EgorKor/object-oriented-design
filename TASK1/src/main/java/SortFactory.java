public class SortFactory {

     public static Isort createSort(String str) throws IllegalAccessException {
        if (str.equalsIgnoreCase("bubble sort")) {
            return new BubbleSort();
        } else {
            if (str.equalsIgnoreCase("shellsort")) {
                return new Shellsort();
            } else {
                if (str.equalsIgnoreCase("Radix sort")) {
                    return new RadixSort();
                } else throw new IllegalAccessException("Error sort");
            }
        }
    }
}
