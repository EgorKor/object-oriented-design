import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortTest {

    @Test
    public void bubbleSortTest() throws IllegalAccessException {
        int [] arr = {1,2,6,7,8,4,2,0};
        int [] ints = {0,1,2,2,4,6,7,8};

        Sort sort = new Sort();
        sort.sort("bubble sort",arr);

        assertArrayEquals(ints,arr);
    }

    @Test
    public void shellSortTest() throws IllegalAccessException {
        int [] arr = {1,2,6,7,8,4,2,0};
        int [] ints = {0,1,2,2,4,6,7,8};

        Sort sort = new Sort();
        sort.sort("shellsort",arr);

        assertArrayEquals(ints,arr);
    }

    @Test
    public void radixSortTest() throws IllegalAccessException {
        int [] arr = {1,2,6,7,8,4,2,0};
        int [] ints = {0,1,2,2,4,6,7,8};

        Sort sort = new Sort();
        sort.sort("radix sort",arr);

        assertArrayEquals(ints,arr);
    }
}