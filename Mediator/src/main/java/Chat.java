public class Chat implements Mediator {

    private TaxiDriver taxiDriver;
    private Passenger passenger;

    public void setTaxiDriver(TaxiDriver taxiDriver) {
        this.taxiDriver = taxiDriver;
    }

    public void addPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public void send(String sms, User user) {
        if (taxiDriver == null || passenger == null || sms.equals("") || sms == null) {
            throw new IllegalArgumentException("Error!");
        }
        if (user == taxiDriver) {
            passenger.getMassage(user.getName(), sms);
        } else {
            taxiDriver.getMassage(user.getName(), sms);
        }
    }
}
