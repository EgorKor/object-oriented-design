public class Passenger implements User {
    private final Mediator mediator;
    private final String name;

    public Passenger(Mediator mediator, String name) {
        if (name == null || name.equals("") || mediator == null) {
            throw new IllegalArgumentException("Error!");
        }
        this.mediator = mediator;
        this.name = name;
    }

    public void sendMassage(String string) {
        if (string == null || string.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        mediator.send(string, this);
    }

    public void getMassage(String name, String sms) {
        System.out.println("Водитель " + name + " отправил сообщение: " + sms + "\n");
    }

    public String getName() {
        return name;
    }
}
