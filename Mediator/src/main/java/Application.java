public class Application {
    public static void main(String[] args) {
        Chat chat = new Chat();
        Passenger passenger = new Passenger(chat, "Иван");
        TaxiDriver taxiDriver = new TaxiDriver(chat, "Андрей");

        chat.addPassenger(passenger);
        chat.setTaxiDriver(taxiDriver);

        passenger.sendMassage("Адрес 15");

        taxiDriver.sendMassage("Буду через пять минут");
    }
}
