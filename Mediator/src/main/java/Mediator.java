public interface Mediator {
     void send(String sms, User user);
}
