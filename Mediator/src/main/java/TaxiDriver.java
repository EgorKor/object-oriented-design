public class TaxiDriver implements User {
    private final Mediator mediator;
    private final String name;

    public TaxiDriver(Mediator mediator, String name) {
        if (mediator == null || name == null || name.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        this.mediator = mediator;
        this.name = name;
    }

    public void sendMassage(String string) {
        if (string == null || string.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        mediator.send(string, this);
    }

    public void getMassage(String string, String sms) {
        if (string == null || string.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        System.out.println("Пассажир " + string + " отправил сообщение: " + sms);
    }

    public String getName() {
        return name;
    }
}
