public class GameOnLinux {

    public void startGameOnLinux(){
        System.out.println("Start Game");
    }

    public void saveGameOnLinux(){
        System.out.println("The game was saved");
    }

    public void newGameOnLinux(){
        System.out.println("Creating a new game");
    }

    public void exitGameOnLinux(){
        System.out.println("Exiting the game");
    }
}
