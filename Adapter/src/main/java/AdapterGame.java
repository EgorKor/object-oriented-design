public class AdapterGame extends GameOnLinux implements GameOnWindows {

    public void start() {
        startGameOnLinux();
    }

    public void save() {
        saveGameOnLinux();
    }

    public void newGame() {
        newGameOnLinux();
    }

    public void exit() {
        exitGameOnLinux();
    }
}
