public class StartGame {

    public static void main(String[] args) {
        GameOnWindows game = new AdapterGame();

        game.start();
        game.newGame();
        game.save();
        game.exit();
    }
}
