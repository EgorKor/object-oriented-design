public class SheepFactory {
    Sheep sheep;

    public SheepFactory(Sheep sheep) {
        this.sheep = sheep;
    }

    public void setSheep(Sheep sheep) {
        this.sheep = sheep;
    }

    Sheep cloneSheep(){
        return (Sheep) sheep.copy();
    }
}

