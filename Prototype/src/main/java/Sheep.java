import java.util.Objects;

public class Sheep implements Prototype{
    private String name;
    private double weight;
    private double height;
    private Color color;

    public Sheep(String name, double weight, double height, Color color) {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }


    public Object copy() {
        return new Sheep(name,weight,height,color);
    }

    @Override
    public String toString() {
        return "Sheep{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                ", color=" + color +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sheep sheep = (Sheep) o;
        return Double.compare(sheep.weight, weight) == 0 &&
                Double.compare(sheep.height, height) == 0 &&
                Objects.equals(name, sheep.name) &&
                color == sheep.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight, height, color);
    }

}
