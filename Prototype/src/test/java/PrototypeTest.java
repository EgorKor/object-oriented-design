import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrototypeTest {

    @Test
    public void sheepCloneTest(){
        Sheep sheep = new Sheep("Dolly",75.5,56,Color.BLACK);

        Sheep clone = (Sheep) sheep.copy();
        assertEquals(sheep,clone);
    }

    @Test
    public void sheepFactoryTest(){
        Sheep sheep = new Sheep("Dolly",75.5,56,Color.WHITE);

        SheepFactory sheepFactory = new SheepFactory(sheep);
        Sheep clone  = sheepFactory.cloneSheep();
        assertEquals(sheep,clone);
    }
}
