/*
Конкретная фабрика, которая производит матрицы диагональю 14 дюймов
 */
public class Matrix14DiagonalFactory implements IMatrixFactory {
    public IIpsMatrix createIPS() {
        return new DiagonalOfTheMatrix14Ips();
    }

    public ITNMatrix createTN() {
        return new DiagonalOfTheMatrix14TN();
    }
}
