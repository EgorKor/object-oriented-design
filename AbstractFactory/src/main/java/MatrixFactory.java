public class MatrixFactory {

    /*
    Выбирается нужная диагональ и создается конкретная фабрика
     */
    public static Application createMatrix(int diagonal) throws IllegalAccessException {
        Application application;
        IMatrixFactory iMatrixFactory;

        if (diagonal == 14) {
            iMatrixFactory = new Matrix14DiagonalFactory();
            application = new Application(iMatrixFactory);
        } else if (diagonal == 16) {
            iMatrixFactory = new Matrix16DiagonalFactory();
            application = new Application(iMatrixFactory);
        } else {
            throw new IllegalAccessException("Error diagonal");
        }
        return application;
    }
}
