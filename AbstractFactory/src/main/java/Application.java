public class Application {
    private final ITNMatrix itnMatrix;
    private final IIpsMatrix iIpsMatrix;

/*
Код, использующий фабрику
 */

    public Application(IMatrixFactory factory) {
        itnMatrix = factory.createTN();
        iIpsMatrix = factory.createIPS();
    }

    public void creatingMatrixDesiredDiagonal(){
        itnMatrix.create();
        iIpsMatrix.create();
    }
}
