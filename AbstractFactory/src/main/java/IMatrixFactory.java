/*
Абстрактная фабрика
 */

public interface IMatrixFactory {
IIpsMatrix createIPS();
ITNMatrix createTN();
}
