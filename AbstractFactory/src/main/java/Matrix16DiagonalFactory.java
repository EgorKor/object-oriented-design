/*
Конкретная фабрика, которая производит матрицы диагональю 14 дюймов
 */
public class Matrix16DiagonalFactory implements IMatrixFactory {

    public IIpsMatrix createIPS() {
        return new DiagonalOfTheMatrix16Ips();
    }

    public ITNMatrix createTN() {
        return new DiagonalOfTheMatrix16TN();
    }
}
