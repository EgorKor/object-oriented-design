import org.junit.Test;

public class MatrixFactoryTest {

    @Test
    public void testDiagonal14() throws IllegalAccessException {
        MatrixFactory.createMatrix(14).creatingMatrixDesiredDiagonal();
    }

    @Test
    public void testDiagonal16() throws IllegalAccessException {
        MatrixFactory.createMatrix(16).creatingMatrixDesiredDiagonal();
    }
}
