import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuilderTest {

    @Test
    public void test1() {
        TypeOfGoods typeOfGoods = new Phone(new Apple());
        assertEquals("Phone Apple", typeOfGoods.showDetails());
    }

    @Test
    public void test2() {
        TypeOfGoods typeOfGoods = new Phone(new Lenovo());
        assertEquals("Phone Lenovo", typeOfGoods.showDetails());
    }

    @Test
    public void test3() {
        TypeOfGoods typeOfGoods = new Laptop(new Huawei());
        assertEquals("Laptop Huawei", typeOfGoods.showDetails());

    }

    @Test
    public void test4() {
        TypeOfGoods typeOfGoods = new Smartwatch(new Lenovo());
        assertEquals("SmartWatch Lenovo", typeOfGoods.showDetails());

    }

    @Test
    public void test5() {
        TypeOfGoods typeOfGoods = new Smartwatch(new Apple());
        assertEquals("SmartWatch Apple", typeOfGoods.showDetails());

    }
}
