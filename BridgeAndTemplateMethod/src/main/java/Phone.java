public class Phone extends TypeOfGoods {
    public Phone(Manufacturer manufacturer) {
        super(manufacturer);
    }

    String show() {
        return "Phone ";
    }
}
