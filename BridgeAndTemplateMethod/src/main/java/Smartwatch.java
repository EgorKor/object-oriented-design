public class Smartwatch extends TypeOfGoods {
    public Smartwatch(Manufacturer manufacturer) {
        super(manufacturer);
    }

    String show() {
        return "SmartWatch ";
    }
}
