public class Laptop extends TypeOfGoods {
    public Laptop(Manufacturer manufacturer) {
        super(manufacturer);
    }

    public String show() {
        return "Laptop "; //manufacturer.showManufacturer(); Здесь это не нужно, так уже реализованно в классе TypeOfGoods
    }
}
