//Класс типов продукции
/**Вызов метода manufacturer.showManufacturer(), позволяет изабиться от вызова
 * его в классах, реализующих различные виды техники.
 * Данного эффекта удалось добиться, реализовав паттер Шаблонный метод
**/
public abstract class TypeOfGoods {
    Manufacturer manufacturer;

    public TypeOfGoods(Manufacturer manufacturer){
        this.manufacturer = manufacturer;
    }

    abstract String show();

    String showDetails(){
        return show()+ manufacturer.showManufacturer();
    }

}
