import com.detectlanguage.errors.APIError;

public class Main {
    public static void main(String[] args) throws APIError {
        Chain one  = new EngLanguage();
        Chain two  = new RuLanguage();
        Chain three  = new CzLanguage();

        one.setNext(two);
        two.setNext(three);

        one.process("Я русский");
        one.process("Hello world");
        one.process("text v Češtině");
    }
}
