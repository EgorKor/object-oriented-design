import com.detectlanguage.errors.APIError;

public interface Chain {
    void setNext(Chain chain);

    void process(String string) throws APIError;

}
