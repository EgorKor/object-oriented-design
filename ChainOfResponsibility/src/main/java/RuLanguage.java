import com.detectlanguage.DetectLanguage;
import com.detectlanguage.errors.APIError;

public class RuLanguage implements Chain {
    private Chain nextChain;

    public void setNext(Chain chain) {
        this.nextChain = chain;
    }

    public void process(String string) throws APIError {
        if(string.equals("")||string==null){
            throw new IllegalArgumentException("Error string");
        }
        DetectLanguage.apiKey = "3ca7b663d4946f0f064645c15965f2c7";
        String language = DetectLanguage.simpleDetect(string);
        if (!language.equals("ru")) {
            nextChain.process(string);
        } else {
            System.out.println("Это русский язык");
        }
    }
}
