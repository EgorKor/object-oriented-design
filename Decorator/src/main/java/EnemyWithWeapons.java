public class EnemyWithWeapons extends EnemyDecorator {

    public EnemyWithWeapons(IEnemy enemy) {
        super(enemy);
    }

    @Override
    public String createEnemy() {
        return super.createEnemy()+addWeapon();
    }

    public String addWeapon(){
        return "Оружие добавлено. ";
    }
}
