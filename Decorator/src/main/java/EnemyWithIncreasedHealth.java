public class EnemyWithIncreasedHealth extends EnemyDecorator {

    public EnemyWithIncreasedHealth(IEnemy enemy) {
        super(enemy);
    }

    @Override
    public String createEnemy() {
        return super.createEnemy()+addHealth();
    }

    public String addHealth(){
        return "Здоровье повышено. ";
    }
}
