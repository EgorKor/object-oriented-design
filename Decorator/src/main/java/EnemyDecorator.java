public class EnemyDecorator implements IEnemy {

    private final IEnemy enemy;

    public EnemyDecorator(IEnemy enemy) {
        this.enemy = enemy;
    }

    public String createEnemy() {
        return enemy.createEnemy();
    }
}
