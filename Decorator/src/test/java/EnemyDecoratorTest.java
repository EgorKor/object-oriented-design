import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EnemyDecoratorTest {

    @Test
    public void test1(){
        IEnemy enemy = new Enemy();
        assertEquals(enemy.createEnemy(),"Создан противник. ");
    }

    @Test
    public void test2(){
        IEnemy enemy = new EnemyWithWeapons(new Enemy());
        assertEquals(enemy.createEnemy(),"Создан противник. Оружие добавлено. ");
    }

    @Test
    public void test3(){
        IEnemy enemy = new EnemyWithIncreasedHealth(new Enemy());
        assertEquals(enemy.createEnemy(),"Создан противник. Здоровье повышено. ");
    }

    @Test
    public void test4(){
        IEnemy enemy = new EnemyWithIncreasedHealth(new EnemyWithWeapons(new Enemy()));
        assertEquals(enemy.createEnemy(),"Создан противник. Оружие добавлено. Здоровье повышено. ");
    }
}
