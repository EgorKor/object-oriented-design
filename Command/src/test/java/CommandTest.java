import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommandTest {

    @Test
    public void test1() {
        ATM atm = new ATM();

        User user = new User(new ToDepositCashCommand(atm),
                new WithdrewCashCommand(atm),
                new MakeATransferCommand(atm),
                new PayForHousingAndUtilitiesCommand(atm));

        assertEquals("Внести наличные",user.toDepositCash.execute());
        assertEquals("Снять наличные",user.withdrawCash.execute());
        assertEquals("Совершить перевод",user.makeATransfer.execute());
        assertEquals("Оплатить ЖКХ",user.payForHousingAndUtilities.execute());

    }
}
