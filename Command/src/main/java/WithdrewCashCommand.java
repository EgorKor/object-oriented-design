public class WithdrewCashCommand implements Command {
    private final ATM atm;

    public WithdrewCashCommand(ATM atm) {
        this.atm = atm;
    }

    public String execute() {
        return atm.withdrawCash();
    }
}
