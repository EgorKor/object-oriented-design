public class PayForHousingAndUtilitiesCommand implements Command {
    private ATM atm;

    public PayForHousingAndUtilitiesCommand(ATM atm) {
        this.atm = atm;
    }

    public String execute() {
        return atm.payForHousingAndUtilities();
    }
}
