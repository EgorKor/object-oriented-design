//Пользователь банкомата
public class User {

    Command toDepositCash;
    Command withdrawCash;
    Command makeATransfer;
    Command payForHousingAndUtilities;


    public User(Command toDepositCash, Command withdrawCash, Command makeATransfer, Command payForHousingAndUtilities) {
        this.toDepositCash = toDepositCash;
        this.withdrawCash = withdrawCash;
        this.makeATransfer = makeATransfer;
        this.payForHousingAndUtilities = payForHousingAndUtilities;
    }

    public String toDepositCash() {
        return toDepositCash.execute();
    }

    public String withdrawCash() {
        return withdrawCash.execute();
    }

    public String makeATransfer() {
        return makeATransfer.execute();
    }

    public String payForHousingAndUtilities() {
        return payForHousingAndUtilities.execute();
    }

}
