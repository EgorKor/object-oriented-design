public class ToDepositCashCommand implements Command {

    private final ATM atm;

    public ToDepositCashCommand(ATM atm) {
        this.atm = atm;
    }

    public String execute() {
        return atm.toDepositCash();
    }
}
