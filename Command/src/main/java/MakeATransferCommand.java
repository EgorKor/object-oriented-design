public class MakeATransferCommand implements Command {
    private final ATM atm;

    public MakeATransferCommand(ATM atm) {
        this.atm = atm;
    }

    public String execute() {
        return atm.makeATransfer();
    }
}
