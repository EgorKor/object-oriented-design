//Банкомат
public class ATM {

    public String toDepositCash() {
        return "Внести наличные";
    }

    public String withdrawCash() {
        return "Снять наличные";
    }

    public String makeATransfer() {
        return "Совершить перевод";
    }

    public String payForHousingAndUtilities() {
        return "Оплатить ЖКХ";
    }


}
