//Класс загрузки и воспроизведения музыки с сайта

public class RealMusic implements Music{

    private final String URL;

    public RealMusic(String URL) {
        this.URL = URL;
        loadMusic();
    }

    public void loadMusic(){
        System.out.println("Загрузка трека с " + URL);
    }

    public void playMusic() {
        System.out.println("Проигрывание трека " + URL);
    }

}
