//Класс заместитель

public class ProxyMusic implements Music {

    private final String URL;
    private RealMusic realMusic;

    public ProxyMusic(String URL) {
        this.URL = URL;
    }

    public void playMusic() {
        if(realMusic == null){
            realMusic = new RealMusic(URL);
        }
       realMusic.playMusic();
    }
}
