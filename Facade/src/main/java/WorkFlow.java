//Facade

public class WorkFlow {
  private final TechnicalSpecification technicalSpecification= new TechnicalSpecification();
  private final Developer developer = new Developer();
  private final SalesDepartment salesDepartment = new SalesDepartment();

    public void developmentOfTheProduct(){
        technicalSpecification.write();
        developer.writeCod();
        salesDepartment.presentationAndSale();
    }
}
