//Клиентский код

public class Concert {
    public static void main(String[] args) {
        MusicianFactory musicianFactory = new MusicianFactory();
        MusicalGroup musicalGroup = new MusicalGroup();

        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("bass"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("guitar"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("guitar"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("vocal"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("vocal"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("vocal"));
        musicalGroup.addMusical(musicianFactory.getMusicianByInstrument("drum"));
        System.out.println("========================================================");

        musicalGroup.createBand();
    }
}
