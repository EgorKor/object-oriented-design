//Класс реализация компоновщика
//Он собирает различные объекты в одну сущность

import java.util.ArrayList;
import java.util.List;

public class MusicalGroup {
    List<Musician> musicians= new ArrayList<Musician>();

    public void addMusical(Musician musician) {
        musicians.add(musician);
    }

    public void removeMusical(Musician musician) {
        musicians.remove(musician);
    }

    public void createBand() {
        for(Musician musician:musicians){
            musician.playMusic();
        }
    }
}
