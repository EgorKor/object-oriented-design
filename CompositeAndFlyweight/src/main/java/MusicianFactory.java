//Класс реализация легковеса
//Он позволяет создать объект каждого вида только один раз

import java.util.HashMap;
import java.util.Map;

public class MusicianFactory {
    Map<String, Musician> musicians = new HashMap<String, Musician>();

    public Musician getMusicianByInstrument(String instrument) {
        if (instrument == null || instrument.equals("")) {
            throw new IllegalArgumentException("Ошибка! Неверный иструмент!");
        }

        Musician musician = musicians.get(instrument);
        if (musician == null) {
            if (instrument.equalsIgnoreCase("guitar")) {
                System.out.println("Создан гитарист...");
                musician = new Guitarist();
            } else {
                if (instrument.equalsIgnoreCase("vocal")) {
                    System.out.println("Создан вокалист...");
                    musician = new Vocalist();
                } else {
                    if (instrument.equalsIgnoreCase("bass")) {
                        System.out.println("Создан басист...");
                        musician = new BassPlayer();
                    } else {
                        if (instrument.equalsIgnoreCase("Drum")) {
                            System.out.println("Создан барабанщик...");
                            musician = new Drummer();
                        } else {
                            throw new IllegalArgumentException("Неверный инструмент");
                        }
                    }
                }
            }
            musicians.put(instrument,musician);
        }
        return musician;
    }
}
