//Сортировщик
public class Sorter {
    Sort sort;

    public void setSort(Sort sort){
        this.sort = sort;
    }

    public void executeSort(int []  arr){
        sort.sort(arr);
    }
}
