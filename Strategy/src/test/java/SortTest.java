import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortTest {
    @Test
    public void testAscendingSort() {
        Sorter sorter = new Sorter();
        int[] sortArr = {0, 1, 1, 4, 5, 6, 9};

        int[] arr = {1, 4, 6, 1, 9, 5, 0};
        sorter.setSort(new AscendingSort());
        sorter.executeSort(arr);

        assertArrayEquals(sortArr, arr);
    }

    @Test
    public void testDescendingSort(){
        Sorter sorter = new Sorter();
        int [] sortArr = {9,6,5,4,1,1,0};

        int [] arr = {1,4,6,1,9,5,0};
        sorter.setSort(new DescendingSort());
        sorter.executeSort(arr);

        assertArrayEquals(sortArr,arr);
    }
}
