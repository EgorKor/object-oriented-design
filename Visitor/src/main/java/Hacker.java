public interface Hacker {

    void visit(SecuritySystem securitySystem);
    void visit(DataBase dataBase);
    void visit(Website website);
}
