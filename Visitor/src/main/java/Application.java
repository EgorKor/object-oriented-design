public class Application {

    public static void main(String[] args) {
        Project project = new Project();

        BlackHatHacker blackHatHacker = new BlackHatHacker();
        WhiteHatHacker whiteHatHacker = new WhiteHatHacker();

        project.accept(blackHatHacker);
        System.out.println("===============================================");
        project.accept(whiteHatHacker);
    }
}
