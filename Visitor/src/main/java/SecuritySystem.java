public class SecuritySystem implements PartOfTheProject {
    public void accept(Hacker hacker) {
        hacker.visit(this);
    }
}
