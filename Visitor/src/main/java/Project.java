public class Project implements PartOfTheProject, Collections{
    private final PartOfTheProject[] project;

    public Project(){
        this.project = new PartOfTheProject[]{
                new SecuritySystem(),new DataBase(), new Website()
        };
    }

    public void accept(Hacker hacker) {
        Iterator iterator = new PartIterator();
        while (iterator.hasNext()){
           PartOfTheProject part = (PartOfTheProject) iterator.next();
           part.accept(hacker);
        }
    }

    public Iterator getIterator() {
        return new PartIterator();
    }

    private class PartIterator implements Iterator{
        int index;

        public boolean hasNext() {
            return index < project.length;
        }

        public Object next() {
            return project[index++];
        }
    }
}
