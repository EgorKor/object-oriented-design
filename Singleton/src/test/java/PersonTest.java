import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonTest {

    @Test
    public void instanceTest() throws IllegalAccessException {
        Person person = Person.getInstance("Name",77);
        Person otherPerson = Person.getInstance("UserName",17);

        assertEquals(person,otherPerson);
    }
}