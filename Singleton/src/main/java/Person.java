public class Person {
    private static Person instance;
    public String name;
    public int age;

    private Person(String name, int age) throws IllegalAccessException {
        if (name.equals("") || name == null) {
            throw new IllegalAccessException("Неверно задано имя");
        }
        if (age < 0) {
            throw new IllegalAccessException("Неверно задан возраст");
        }
        this.name = name;
        this.age = age;
    }

    public static Person getInstance(String name, int age) throws IllegalAccessException {
        if (name.equals("") || name == null) {
            throw new IllegalAccessException("Неверно задано имя");
        }
        if (age < 0) {
            throw new IllegalAccessException("Неверно задан возраст");
        }
        if (instance == null) {
            instance = new Person(name, age);
        }
        return instance;
    }
}
