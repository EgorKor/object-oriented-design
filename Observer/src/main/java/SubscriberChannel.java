//Подписчик
public class SubscriberChannel implements Subscriber{
    public void notification(String s, String  youTubeChannelName) {
        if(s.equals("")||youTubeChannelName.equals("")||s==null||youTubeChannelName==null){
            throw new IllegalArgumentException("Error!");
        }
        System.out.println("Новое видео на канале "+youTubeChannelName+": "+s+". ");
    }
}
