//Пример ютуб канала #2

import java.util.ArrayList;
import java.util.List;

public class JavaIsSimple implements YouTubeChannel {

    private final List<Subscriber> subscriberList = new ArrayList<Subscriber>();
    private final List<String> listVideo = new ArrayList<String>();

    public void addSubscriber(Subscriber subscriber) {
        if(subscriber==null){
            throw new IllegalArgumentException("Error!");
        }
        this.subscriberList.add(subscriber);
    }

    public void removeSubscriber(Subscriber subscriber) {
        if(subscriber==null){
            throw new IllegalArgumentException("Error!");
        }
        this.subscriberList.remove(subscriber);
    }

    public void subscribersNotification() {
        for(Subscriber subscriber: subscriberList){
            subscriber.notification(listVideo.get(listVideo.size()-1),JavaIsSimple.class.getName());
        }
    }

    public void addNewVideo(String string){
        if(string==null||string.equals("")){
            throw new IllegalArgumentException("Error!");
        }
        listVideo.add(string);
        subscribersNotification();
    }
}
