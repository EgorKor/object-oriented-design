public interface YouTubeChannel {

    void addSubscriber(Subscriber subscriber);

    void removeSubscriber(Subscriber subscriber);

    void subscribersNotification();
}
