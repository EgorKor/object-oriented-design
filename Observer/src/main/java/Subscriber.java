//Интерфейс Observer
public interface Subscriber {

    void notification(String s, String youTubeChannel);
}
