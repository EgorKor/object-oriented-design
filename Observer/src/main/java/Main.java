public class Main {
    public static void main(String[] args) {
        ExtremeCode extremeCode =new ExtremeCode();

        Subscriber subscriber = new SubscriberChannel();
        Subscriber subscriber1 = new SubscriberChannel();
        Subscriber subscriber2 = new SubscriberChannel();
        Subscriber subscriber3 = new SubscriberChannel();

        extremeCode.addSubscriber(subscriber);
        extremeCode.addSubscriber(subscriber1);
        extremeCode.addSubscriber(subscriber2);
        extremeCode.addNewVideo("Принципы S.O.L.I.D");
        System.out.println("\n================================================\n");
        JavaIsSimple javaIsSimple  =new JavaIsSimple();
        javaIsSimple.addSubscriber(subscriber);
        javaIsSimple.addSubscriber(subscriber3);
        javaIsSimple.addNewVideo("Java с 0 до профессионала за 1 час");

    }
}
