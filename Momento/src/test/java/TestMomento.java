import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMomento {

    @Test
    public void testSave() {
        Character character = new Character(100, 12, "Ok");
        Character character1 = new Character(100, 12, "Ok");

        PlaceOfRetention placeOfRetention = new PlaceOfRetention();
        placeOfRetention.setSave(character.save());

        character.setHp(12);
        character.setTime(20);
        character.setCondition("Bad");
        character.load(placeOfRetention.getSave());
        assertEquals(character1, character);
    }
}
