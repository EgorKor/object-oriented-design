public class PlaceOfRetention {

    private Save save;

    public Save getSave() {
        return save;
    }

    public void setSave(Save save) {
        if (save == null) {
            throw new IllegalArgumentException("Error!");
        }
        this.save = save;
    }
}
