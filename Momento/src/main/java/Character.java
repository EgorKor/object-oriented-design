import java.util.Objects;

public class Character {
    private int hp;
    private int time;
    private String condition;

    public Character(int hp, int time, String condition) {
        setHp(hp);
        setTime(time);
        setCondition(condition);
    }

    public void setHp(int hp) {
        if (hp < 0 || hp > 100) {
            throw new IllegalArgumentException("Error!");
        }
        this.hp = hp;
    }

    public void setTime(int time) {
        if (time < 0) {
            throw new IllegalArgumentException("Error!");
        }
        this.time = time;
    }

    public void setCondition(String condition) {
        if (condition == null || condition.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        this.condition = condition;
    }

    public Save save() {
        return new Save(hp, time, condition);
    }

    public void load(Save save) {
        hp = save.getHp();
        time = save.getTime();
        condition = save.getCondition();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Character character = (Character) o;
        return hp == character.hp &&
                time == character.time &&
                Objects.equals(condition, character.condition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hp, time, condition);
    }
}
