public class Save {
    private final int hp;
    private final int time;
    private final String condition;

    public Save(int hp, int time, String condition) {
        if (hp < 0 || hp > 100) {
            throw new IllegalArgumentException("Error!");
        }
        if (time < 0) {
            throw new IllegalArgumentException("Error!");
        }
        if (condition == null || condition.equals("")) {
            throw new IllegalArgumentException("Error!");
        }
        this.hp = hp;
        this.time = time;
        this.condition = condition;
    }

    public int getHp() {
        return hp;
    }

    public int getTime() {
        return time;
    }

    public String getCondition() {
        return condition;
    }
}
