public interface Builder {

    void setLevel(Level level);
    void setTime(int time);
    void setName(String name);
    void setNumberOpponents(int numberOpponents);
    void setWeapon(Weapon weapon);
}
