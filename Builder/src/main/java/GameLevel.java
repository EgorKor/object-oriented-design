public class GameLevel {

    private final Level level;
    private final int time;
    private final String name;
    private final int numberOpponents;
    private  Weapon weapon;

    public GameLevel(Level level, int time, String name, int numberOpponents, Weapon weapon) {
        this.level = level;
        this.time = time;
        this.name = name;
        this.numberOpponents = numberOpponents;
        if (weapon != null) {
            this.weapon = weapon;
        }
    }

    public Level getLevel() {
        return level;
    }

    public int getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public int getNumberOpponents() {
        return numberOpponents;
    }

    public Weapon getWeapon() {
        return weapon;
    }
}
