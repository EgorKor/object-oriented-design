public class GameLevelBuilder implements Builder {

    private Level level;
    private int time;
    private String name;
    private int numberOpponents;
    private Weapon weapon;


    public void setLevel(Level level) {
        this.level = level;
    }

    public void setTime(int time) {
        this.time = time;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOpponents(int numberOpponents) {
        this.numberOpponents = numberOpponents;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public GameLevel getResult() {
        return new GameLevel(level, time, name, numberOpponents,weapon);
    }
}
