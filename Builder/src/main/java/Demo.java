public class Demo {
    public static void main(String[] args) {
        Director director = new Director();

        GameLevelBuilder gameLevelBuilder = new GameLevelBuilder();
        director.easyGameLevel(gameLevelBuilder);
        System.out.println("Сreated a game with a level:"+gameLevelBuilder.getResult().getLevel());


        GameLevelBuilder gameLevelBuilder1 = new GameLevelBuilder();
        director.middleGameLevel(gameLevelBuilder1);
        System.out.println("Сreated a game with a level:"+gameLevelBuilder1.getResult().getLevel());


        GameLevelBuilder gameLevelBuilder2 = new GameLevelBuilder();
        director.hardGameLevel(gameLevelBuilder2);
        System.out.println("Сreated a game with a level:"+gameLevelBuilder2.getResult().getLevel());


        GameLevelBuilder gameLevelBuilder3 = new GameLevelBuilder();
        director.extraGameLevel(gameLevelBuilder3);
        System.out.println("Сreated a game with a level:"+gameLevelBuilder3.getResult().getLevel());
    }
}
