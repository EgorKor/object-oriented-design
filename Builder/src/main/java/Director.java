public class Director {

    public void easyGameLevel(Builder builder){
        builder.setLevel(Level.EASY);
        builder.setTime(100);
        builder.setName("One");
        builder.setNumberOpponents(3);
        builder.setWeapon(Weapon.SWORD);
    }

    public void middleGameLevel(Builder builder){
        builder.setLevel(Level.MIDDLE);
        builder.setTime(80);
        builder.setName("Two");
        builder.setNumberOpponents(4);
        builder.setWeapon(Weapon.AXE);
    }

    public void hardGameLevel(Builder builder){
        builder.setLevel(Level.HARD);
        builder.setTime(60);
        builder.setName("Three");
        builder.setNumberOpponents(5);
        builder.setWeapon(Weapon.KNIFE);
    }

    public void extraGameLevel(Builder builder){
        builder.setLevel(Level.EXTRA);
        builder.setTime(60);
        builder.setName("Four");
        builder.setNumberOpponents(10);
    }
}

